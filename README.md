## This is the [hast](https://bitbucket.org/vperret/hast) bitbucket repository.

Hast is an open source code to select halo of interest in Ramses unigrid cosmological simulations. Hast uses pynbody to read the simulation data, finds the halos thanks to Ramses clump finder, and computes the volume and density in the initial conditions of the convex hull defining the Lagrangian region.


Download the code by cloning the git repository using 
```
$ git clone https://bitbucket.org/vperret/hast
```